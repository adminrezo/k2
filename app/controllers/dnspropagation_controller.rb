# frozen_string_literal: true

class DnspropagationController < ApplicationController
  def index; end

  def show
    @fqdn = params[:fqdn]
    dp = DnsPropagationJob.new(@fqdn)
    hash = dp.explore
    @hash = hash.to_a
  end
end
