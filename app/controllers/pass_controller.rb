# frozen_string_literal: true

# Password generator
class PassController < ApplicationController
  # GET /tools/password
  def index; end

  # POST /tools/password
  def show
    affect_params
    @pass = gen_pass
  end

  private

  def gen_pass
    pwgen = PawGen.new
    @lngt = 12 if @lngt < 8
    @lngt = 64 if @lngt > 64
    pwgen.set_length!(@lngt)
    pwgen.no_digits! unless @numb
    pwgen.no_uppercase! unless @upp
    pwgen.include_symbols! if @spec
    pass = []
    5.times { pass << pwgen.anglophonemic }
    pass
  end

  def affect_params
    @lngt = params[:len].to_i
    @upp = params[:upp]
    @numb = params[:num]
    @spec = params[:spec]
  end
end
