# frozen_string_literal: true

class Port < ApplicationRecord
  belongs_to :domain
end
