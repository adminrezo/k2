# frozen_string_literal: true

class Subdomain < ApplicationRecord
  belongs_to :domain
end
