# frozen_string_literal: true

class Jslib < ApplicationRecord
  belongs_to :domain
end
