# frozen_string_literal: true

class Header < ApplicationRecord
  belongs_to :domain
end
