# frozen_string_literal: true

class Domain < ApplicationRecord
  validates :name, hostname: true
  belongs_to :user
  has_many :cms, dependent: :destroy
  has_many :headers, dependent: :destroy
  has_many :jslibs, dependent: :destroy
  has_many :mailconfs, dependent: :destroy
  has_many :ports, dependent: :destroy
  has_many :subdomains, dependent: :destroy
  has_many :tlsconfigs, dependent: :destroy
end
