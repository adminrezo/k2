# frozen_string_literal: true

class Mailconf < ApplicationRecord
  belongs_to :domain
end
