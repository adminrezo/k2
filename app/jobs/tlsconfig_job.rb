# frozen_string_literal: true

class TlsconfigJob < ApplicationJob
  queue_as :default

  def perform(domain)
    @domain = domain
    @fn = "tmp/k2/#{@domain.name}.tls"
    @script = 'ssl-enum-ciphers'
    @myhash = ['SSLv1' => 0, 'SSLv2' => 0, 'SSLv3' => 0, 'TLSv1.0' => 4, 'TLSv1.1' => 4, 'TLSv1.2' => 10, 'TLSv1.3' => 10]
    scan
    parse
  end

  private

  def scan
    require 'nmap/command'
    Nmap::Command.run do |nmap|
      nmap.ports = [443]
      nmap.script = @script
      nmap.targets = @domain.name
      nmap.output_xml = @fn
    end
  end

  def parse
    require 'nmap/xml'
    Nmap::XML.open(@fn) do |xml|
      xml.each_host do |host|
        host.ports[0].scripts.each_value { |value| @data = value.output }
      end
    end
    @myhash[0].each do |vers, nt|
      next unless @data.include?(vers)

      Tlsconfig.create(version: vers, note: nt, domain: @domain)
    end
  end

  def note
    @nt = 0
    @nt = 4 if @score.match 'C'
    @nt = 6 if @score.match 'B'
    @nt = 10 if @score.match 'A'
  end
end
