# frozen_string_literal: true

class DigJob < ApplicationJob
  queue_as :default

  def perform(fqdn)
    dnstype = %w[A AAAA CAA CNAME MX NS PTR SOA SRV TXT]
    result = {}
    dnstype.each do |thetype|
      result[thetype] = []
      res = Dnsruby::DNS.new(timeouts: 1)
      res.each_resource(fqdn, thetype) do |rr|
        result[thetype].push(rr.rdata.to_s)
      end
    end
    result
  end
end
