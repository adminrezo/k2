#!/bin/sh

cd /home/k2 || exit 255
. ./.env
./bin/rails db:prepare || exit 255
./bin/rails db:seed
./bin/rails assets:precompile
./bin/rails webpacker:compile
./bin/rails server & # --log-to-stdout
/etc/init.d/postfix start &

wait
exit $?
