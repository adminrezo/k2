# frozen_string_literal: true

class AddUserToDomains < ActiveRecord::Migration[6.1]
  def change
    add_reference :domains, :user, foreign_key: true
  end
end
