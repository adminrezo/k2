# frozen_string_literal: true

class CreateSubdomains < ActiveRecord::Migration[6.1]
  def change
    create_table :subdomains do |t|
      t.string :name

      t.timestamps
    end
  end
end
