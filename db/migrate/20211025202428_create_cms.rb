# frozen_string_literal: true

class CreateCms < ActiveRecord::Migration[6.1]
  def change
    create_table :cms do |t|
      t.string :name
      t.float :version

      t.timestamps
    end
  end
end
