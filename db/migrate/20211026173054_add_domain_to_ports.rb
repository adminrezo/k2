# frozen_string_literal: true

class AddDomainToPorts < ActiveRecord::Migration[6.1]
  def change
    add_column :ports, :domain, :string
    add_reference :ports, :domain, foreign_key: true
  end
end
