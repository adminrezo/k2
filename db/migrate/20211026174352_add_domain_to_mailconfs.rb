# frozen_string_literal: true

class AddDomainToMailconfs < ActiveRecord::Migration[6.1]
  def change
    add_column :mailconfs, :domain, :string
    add_reference :mailconfs, :domain, foreign_key: true
  end
end
