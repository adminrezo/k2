# frozen_string_literal: true

class AddDomainToTlsconfigs < ActiveRecord::Migration[6.1]
  def change
    add_column :tlsconfigs, :domain, :string
    add_reference :tlsconfigs, :domain, foreign_key: true
  end
end
