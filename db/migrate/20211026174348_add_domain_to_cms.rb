# frozen_string_literal: true

class AddDomainToCms < ActiveRecord::Migration[6.1]
  def change
    add_column :cms, :domain, :string
    add_reference :cms, :domain, foreign_key: true
  end
end
