# frozen_string_literal: true

class AddNoteToScan < ActiveRecord::Migration[6.1]
  def change
    add_column :headers, :note, :integer
    add_column :cms, :note, :integer
    add_column :jslibs, :note, :integer
    add_column :mailconfs, :note, :integer
    add_column :ports, :note, :integer
    add_column :subdomains, :note, :integer
    add_column :tlsconfigs, :note, :integer
  end
end
