# frozen_string_literal: true

class AddDomainToJslibs < ActiveRecord::Migration[6.1]
  def change
    add_column :jslibs, :domain, :string
    add_reference :jslibs, :domain, foreign_key: true
  end
end
