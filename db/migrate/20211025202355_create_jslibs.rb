# frozen_string_literal: true

class CreateJslibs < ActiveRecord::Migration[6.1]
  def change
    create_table :jslibs do |t|
      t.string :library
      t.float :version

      t.timestamps
    end
  end
end
