# frozen_string_literal: true

class ChangeVersionToStringInCms < ActiveRecord::Migration[6.1]
  def up
    change_column :cms, :version, :string
  end

  def down
    change_column :cms, :version, :float
  end
end
